package main

type Channel struct {
	clients map[*Client]bool
	entering chan *Client
	leaving chan *Client
	broadcast chan Message
}

func newChannel() *Channel {
	return &Channel{
		clients: make(map[*Client]bool),
		entering: make(chan *Client),
		leaving: make(chan *Client),
		broadcast: make(chan Message),
	}
}

func (ch *Channel) hasUID(s string) (bool, *Client) {
	for client := range ch.clients {
		if client.userID == s { return true, client }
	}
	return false, nil 
}

func (ch *Channel) launch() {
	for {
		select{
		case client := <-ch.entering:
			ch.clients[client] = true
		case client := <-ch.leaving:
			if _, ok := ch.clients[client]; ok {
				delete(ch.clients, client)
				close(client.receiverBuf)
			}
		case message := <-ch.broadcast:
			for client := range ch.clients {
				if client != message.from {
					select {
					case client.receiverBuf <- message:
					default:
						close(client.receiverBuf)
						delete(ch.clients, client)
				}
				}
			}
		}
	}
}