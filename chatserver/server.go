package main

import (
	"fmt"
	"log"
	"net"
	"bufio"
)

func main() {
	listener, err := net.Listen("tcp", "localhost:8000")
	if err != nil {
		log.Fatal(err)
	}
	mainChannel := newChannel()
	go mainChannel.launch()
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Println(err)
			continue
		}
		go handleConn(mainChannel, conn)
	}
}

func handleConn(ch *Channel, conn net.Conn) {
	input := bufio.NewScanner(conn)
	var userID string
	isValidUID := func() bool {
		userID = input.Text()
		if len(userID) < 2 { return false }
		ok, _ := ch.hasUID(userID)
		return !ok
	}
	for ok := true; ok; ok = !isValidUID() {
		fmt.Fprint(conn, "Please enter a valid user id: ")
		input.Scan()
	}
	fmt.Fprintf(conn, "\nWelcome to the chat server\n")
	fmt.Fprintf(conn, "Use @username to send private messages\n")
	enteringMsg := conn.RemoteAddr().String() + " has joined the chat"
	client := newClient(userID, conn)
	client.channels["main"] = ch
	ch.entering <- client
	ch.broadcast <- newMessage(enteringMsg, client)
}
