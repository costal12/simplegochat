package main

import (
	"net"
	"bufio"
	"fmt"
	"strings"
	"math/rand"
	"time"
)

type messageType int
const (
	chat messageType = iota
	status
)

var CAPACITY = 10

type Message struct {
	messageType
	text string
	from *Client
}

func newMessage(msg string, from *Client) Message {
	msg = from.userID + ": " + msg
	return Message{
		text: msg,
		from: from,
	}
}


type Client struct {
	userID string
	publicKey int
	conn net.Conn
	channels map[string]*Channel
	receiverBuf chan Message
}

func newClient(userID string, conn net.Conn) *Client {
	rand.Seed(time.Now().UnixNano())
	client := &Client{
		userID: userID,
		publicKey: rand.Intn(10000),
		conn: conn,
		channels: make(map[string]*Channel),
		receiverBuf: make(chan Message, CAPACITY),
	}
	go client.reader()
	go client.writer()
	return client
}

func (c *Client) leave() {
	mainChannel := c.channels["main"]
	leavingMsg := c.conn.RemoteAddr().String() + " has left the chat"
	mainChannel.leaving <- c
	mainChannel.broadcast <- newMessage(leavingMsg, c)
	c.conn.Close()
}

func (c *Client) reader() {
	// add a ticker for idleness
	defer c.leave()
	buf := bufio.NewScanner(c.conn)
	for buf.Scan() {
		msg := buf.Text()
		fmt.Println("send mesg")
		if strings.Contains(msg, "@") {
			msgFrom := msg[strings.Index(msg, "@")+1:]
			otherUser := strings.Split(msgFrom, " ")[0]
			if ok, otherClient := c.channels["main"].hasUID(otherUser); ok {
				pChannelName := c.publicKey + otherClient.publicKey
				var privateChannel *Channel
				if privateChannel = c.channels[fmt.Sprint(pChannelName)];
				privateChannel == nil {
					privateChannel = newChannel()
					go privateChannel.launch()
					privateChannel.entering <- c
					privateChannel.entering <- otherClient
				}
				privateChannel.broadcast <- newMessage(msg, c)
				continue
			}
		}
		c.channels["main"].broadcast <- newMessage(msg, c)
	}
}

func (c *Client) writer() {
	for msg := range c.receiverBuf {
		fmt.Fprintln(c.conn, msg.text)
	}
}