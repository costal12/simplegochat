# README #

### Completion time ###

The process in total took about 5-7 hours. One 1 hour session to decide on course of action, and about two 2/3 hour sessions to draft the code, experiment, revise and finalize with a simpler option

### What is this repository for? ###

* This project is to valid knowledge in minor areas of networking (concurrency and tcp connectivity)
* Version: 1.0

### Assumptions ###

* Broader array of features were planned, as reflected in the encapsulation of channel processes. This was set to be a conduit for various status updates, and not just chat messages.
	- Unfortunately available time was not sufficient due to extraneous obligations to support these feature plans.
* Scaling optimizations via polling and pooling for status features would support both high frequency at discrete times; this was subject to addition, if time alloted permitted.